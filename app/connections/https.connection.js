/**
 * Created by Himal Liyanage on 10/19/2016.
 */
"use strict";
const https = require('https');
module.exports = class HTTPSConnection {
  constructor() {
  }

  sendRequest(data, options) {
    return new Promise(function(resolve, reject) {
      // request object
      var req = https.request(options, function(res) {
        var result = '';
        res.on('data', function(chunk) {
          result += chunk;
        });
        res.on('end', function() {
          resolve(result);
        });
        res.on('error', function(err) {
          reject(err);
        })
      });
      // req error
      req.on('error', function(err) {
        reject(err);
      });
      //send request witht the postData form
      req.write(data);
      req.end();

    });
  }

}
