/**
 * Created by Himal Randeniya on 11/15/2016.
 */

'use strict';
var HTTPSConnection = require('../connections/https.connection');
const httpsConnection = new HTTPSConnection();

var MainService = require('../services/main.service');
const mainService = new MainService(httpsConnection);

var express = require('express');
var router = express.Router();

var config = require('config');
router.post(config.get('component.route'), function (req, res, next) {
  mainService.init(req)
    .then(() => mainService.fetchInputs(req, res))
    .then(mainService.executeComponent)
    .then(mainService.pushOutputs)
    .catch(handleError)
    .finally(mainService.dispose);
});

function handleError(err) {
  //these are errors related to our build pack
};

module.exports = function (app) {
  //app.use(config.get('account.route.base'), router);
};