/**
 * Created by Himal Randeniya on 11/15/2016.
 */
'use strict';

var TrackerService = require('./tracker.service.js');

module.exports = class ComponentService {


  constructor(componentID) {
    this.componentID = componentID;
    this.trackerService = new TrackerService();//starts tracking
  }

  execute() {
    return new Promise(function (resolve, reject) {
        try {
          component.main(resolve, reject);
        } catch (err) {
          reject(err);
        }
      })
      .then(function () {
        this.trackerService.end(null);
      })
      .catch(function (err) {
        this.trackerService.end(err);
      })
      .finally(function () {});
  }
}