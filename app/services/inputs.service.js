/**
 * Created by Himal Randeniya on 11/15/2016.
 */
'use strict';

var fs = require('fs');
var inputConfig = require('component.json')['inputs'];

module.exports = class InputsService {

    constructor() {}

    execute(req, res) {
        var inputs = processInputs(req);
        return validateInputs(inputs)
            .then(() => saveInputs(inputs))
            .then(() => sendResponse(res));
    }

    //------------------------------------------------------
    //                PRIMARY FUNCTIONS
    //------------------------------------------------------ 

    processInputs(req) {
        return req.body.inputs; //assume its just a json {key: value}
    }

    validateInputs(inputs) {
        //validate that the inputs are okay... 
        return new Promise(function (resolve, reject) {
            //do the validation here against the expected
            //type, etc
            //populate the err if there is an error
            var errs = [];
            Object.keys(inputConfig).forEach((key) => {
                var value = inputs[key];
                if (Object.keys(value).indexOf('type') != -1) {
                    //check to see whether input stream contains value for key
                    if (inputs.indexOf(key) == -1) {
                        errs.push(new Error("Inputs stream does not contain required key, " + key + "."));
                        //TODO: this is a major error from the data stream, we must log this to oops
                    } else {
                        //contains the type key, which is mandatory
                        var type = value['type'];
                        //evaluate the type against the value
                        var result = evaluateType(type, inputs[key]);
                        if (!result.success) {
                            errs.push(new Error("Value for component key did not match type. Expected type: " + result.expectedType + ". Type received: " + result.inputType + "."));
                        } else if (!result.inputType) {
                            errs.push(new Error("Value for component is unexpected and not supported. Expected type: " + result.expectedType + "."));
                        }
                    }
                } else {
                    errs.push(new Error("Component configurations for input, " + key + "does not contain a type."));
                }
            });

            (errs.length > 0) ? reject(errs): resolve()
        });
    }

    saveInputs(inputs) {
        return new Promise(function (resolve, reject) {
            //create inputs map
            var map = createInputMap(inputConfig);
            //write input map
            fs.writeFileSync(config.get('config.map'), map);
            //save inputs to filesystem
            Object.keys(map).forEach((key) => {
                const filepath = map[key];
                var data = inputs[key];
                const type = inputConfig[key]["type"];
                if (type != 'file') {
                    //construct a json
                    data = JSON.stringify({
                        value: data
                    });
                }
                //now write
                //TODO: need to write this to handle the multiple callbacks
                fs.writeFile(filepath, data, "utf8", (err) => {
                    err ? reject(err) : resolve();
                });

            });
        });

    }

    sendResponse(res) {
        return new Promise(function (resolve, reject) {
            res.sendStatus(200).end(); //if we get to here, everything is good
            resolve();
        })
    }

    //------------------------------------------------------
    //                VALIDATE FUNCTIONS
    //------------------------------------------------------ 

    evaluateType(type, input) {
        const inputType = determineObjectType(input);
        return {
            success: type == inputType,
            expectedType: type,
            inputType: inputType
        };
    }

    determineObjectType(object) {
        if (typeof object != 'object') {
            return typeof value;
        } else {
            if (object instanceof Date) {
                return 'date';
            } else if (object instanceof Buffer) {
                //TODO: determine the type of a file
            } else {
                return null
            }
        }
    }

    //------------------------------------------------------
    //                SAVE FUNCTIONS
    //------------------------------------------------------ 

    createInputMap(inputConfig) {
        var inputMap = {};
        Object.keys(inputConfig).forEach((key) => {
            var inputObject = inputs[key];
            var extension = (inputObject.indexOf('extension') != -1) ? inputObject['extension'] : "json"; //primitive objects are stored in json under key value
            var filepath = key + "." + extension;
            inputMap[key] = filepath;
        });
        return inputMap;
    }

}