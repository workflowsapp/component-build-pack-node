/**
 * Created by Himal Randeniya on 11/15/2016.
 */
'use strict';

module.exports = class DisposeService {

  constructor() {}

  execute() {
    cleanUpInputs();
    cleanUpOutputs();
    cleanUpConfig();
  }

  //------------------------------------------------------
  //                CLEAN UP
  //------------------------------------------------------

  cleanUpInputs() {
    //TODO: delete all files in the inputs folder
  }

  cleanUpOutputs() {
    //TODO: delete all files in the outputs folder
  }

  cleanUpConfig() {
    //TODO: delete all misc configuration files
  }

}