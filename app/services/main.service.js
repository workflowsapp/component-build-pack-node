/**
 * Created by Himal Randeniya on 11/15/2016.
 */
'use strict';

const component = require('../component.js');

var InitService = require('./init.service.js');
const initService = new InitService();

var InputsService = require('./inputs.service.js');
const inputsService = new InputsService();

var ComponentService = require('./component.service.js');
const componentService = new ComponentService();

var OutputService = require('./outputs.service.js');
const outputService = new OutputService();

var DisposeService = require('./dispose.service.js');
const disposeService = new DisposeService();


//------------------------------------------------------
//                        INIT
//------------------------------------------------------ 

exports.init = function (req) {
  return initService.execute(req);
}

//------------------------------------------------------
//                 FETCH INPUTS
//------------------------------------------------------ 

exports.fetchInputs = function (req, res) {
  return inputsService.execute(req, res);
}

//------------------------------------------------------
//                EXECUTE COMPONENT
//------------------------------------------------------ 

exports.executeComponent = function () {
  return componentService.execute();
}

//------------------------------------------------------
//                   PUSH OUTPUTS
//------------------------------------------------------ 

exports.pushOutputs = function () {
  return outputService.execute(); 
}

//------------------------------------------------------
//                    DISPOSE
//------------------------------------------------------ 

exports.dispose = function () {
  //clean up, ready for new component loop
  disposeService.execute();
}