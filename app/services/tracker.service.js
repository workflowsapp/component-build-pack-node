/**
 * Created by Himal Randeniya on 11/15/2016.
 */
'use strict';

var componentConfig = require("../component.json")
var fs = require('fs');
var config = require('config');

module.exports = class TrackerService {

    constructor(componentID) {
        this.componentID = componentID;
        this.startTime = new Date();
        this.endTime = null;
        this.whitelistDomain = componentConfig.whitelist_domains || null;
        this.watchPaths = [config.get('component.config')];
        //begin watching
        watchFiles();
    }

    //------------------------------------------------------
    //                PRIMARY FUNCTIONS
    //------------------------------------------------------ 



    //------------------------------------------------------
    //                END
    //------------------------------------------------------ 

    end(err) {
        //stop watching the files
        unwatchFiles();
        //store end time
        this.endTime = new Date();
        //send tracker data
        writeTrackerData(err);
    }

    //------------------------------------------------------
    //                SEND
    //------------------------------------------------------ 

    writeTrackerData(err) {
        var data = {
            componentID: this.componentID, 
            start_time: this.startTime,
            end_time: this.endTime,
            error: err,
        };
        var filepath = config.get('component.tracker');
        fs.writeFile(filepath, JSON.stringify(data), "utf8", (err) => {
            watchConfig(filepath);
            err ? reject(err) : resolve();
        });
    }

    //------------------------------------------------------
    //                WATCHERS
    //------------------------------------------------------ 

    watchFiles() {
        //to prevent tampering with the component config 
        this.watchPaths.forEach((path) => {
            fs.watchFile(filepath, (curr, prev) => {
                if (curr.mtime != prev.mtime) {
                    //the file has been modified!
                    //TODO: call an error -> the fact that the config file has been manipulated
                } else {
                    //file was accessed!!
                    //TODO: call an error -> the file was accessed
                }
            });
        });

    }

    unwatchFiles() {
        this.watchPaths.forEach((path) => {
            fs.unwatchFile(filepath);
        });
    }

}