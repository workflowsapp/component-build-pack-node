/**
 * Created by Himal Randeniya on 11/15/2016.
 */
'use strict';

var fs = require('fs');
var config = require('config');

module.exports = class InitService {

  constructor() {}

  execute(req) {
    return new Promise(function (resolve, reject) {
      var config = getConfig(req);
      validateConfig(config, reject);
      storeConfigurations(config)
        .then(resolve)
        .catch(reject)
    })
  }

  //------------------------------------------------------
  //                PRIMARY FUNCTIONS
  //------------------------------------------------------

  getConfig(req) {

    var config = {};

    //TODO: get the configuration details, including:
    //component id
    //component type
    //component version

    return config;

  }

  validateConfig(config, reject) {

    //only reject the promise and send an error if one of the checks fail

    //TODO: check that correct component was called
    //TODO: check that the version of the component is correct
    //TODO: check that inputs are expected


  }

  storeConfig(config) {
    return new Promise(function (resolve, reject) {
      var filepath = config.get('component.config');
      fs.writeFile(filepath, JSON.stringify(config), "utf8", (err) => {
        watchConfig(filepath);
        err ? reject(err) : resolve();
      });
    })
  }


}