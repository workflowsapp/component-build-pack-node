var express = require('express');
var config = require('config');
var app = express();
require('./config/express')(app);
var serverPort=process.env.PORT || config.get('server.port')
app.listen(serverPort, function () {
  console.log('Express server listening on port ' + serverPort);
});

